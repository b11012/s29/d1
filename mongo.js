/*
		Mini Activity : 5 mins
		>> Create a new database called session29
		>> Open a shell in your new database called session29

		>> Insert 5 products in a new products collection with the following details:
			
			name: Iphone X
			price: 30000
			isActive: true

			name: Samsung Galaxy S21
			price: 51000
			isActive: true

			name: Razer Blackshark V2X
			price: 2800
			isActive: false

			name: RAKK Gaming Mouse
			price: 1800
			isActive: true

			name: Razer Mechanical Keyboard
			price: 4000
			isActive: true

	*/

// S O L U T I O N

	db.products.insertMany([
			{
				name: "Iphone X",
				price: 30000,
				isActive: true
			},
			{
				name: "Samsung Galaxy S21",
				price: 51000,
				isActive: true
			},
			{
				name: "Razer Blackshark V2X",
				price: 2800,
				isActive: false
			},
			{
				name: "RAKK Gaming Mouse",
				price: 1800,
				isActive: true
			},
			{
				name: "Razer Mechanical Keyboard",
				price: 4000,
				isActive: true
			}
		])

// Query Operators and Field Projection

// Query Operators
	// for flexible querying in MongoDB
	// instead of finding exact values, we can use query operators that define conditions instead of just specific criterias

// $gt, $lt, $gte and $lte

	// $gt: greater than

	db.products.find( {price: {$gt: 3000} } );

	// $lt: less than

	db.products.find( {price: {$lt: 4000} } )

	// $gte: greater than or equal

	db.products.find( {price: {$gte: 30000} } )

	//$lte: less than or equal

	db.products.find( {price: {$lte: 2800} } )

	// expanding the search criteria for updating or deleting

	db.products.updateMany(
			{
				price: {$gte: 30000}
			},
			{
				$set: {isActive: false}
			}

		)

/*
	Mini Activity: 5 mins

	>> Insert 5 users in a new users collection with the following details:

	firstName: Mary Jane
	lastName: Watson
	email: mjtiger@gmail.com
	password: tigerjackpot15
	isAdmin: false

	firstName: Gwen
	lastName: Stacy
	email: stacyTech@gmail.com
	password: stacyTech1991
	isAdmin: true

	firstName: Peter
	lastName: Parker
	email: peterWebDev@gmail.com
	password: webDeveloperPeter
	isAdmin: true

	firstName: Jonah
	lastName: Jameson
	email: jjjameson@gmail.com
	password: spideyisamenace
	isAdmin: false

	firstName: Otto
	lastName: Octavius
	email: ottoOctopi@gmail.com
	password: docOck15
	isAdmin: true

*/

// S O L U T I O N

db.users.insertMany([
	{
		firstName: "Mary Jane",
		lastName: "Watson",
		email: "mjtiger@gmail.com",
		password: "tigerjackpot15",
		isAdmin: false
	},

	{
		firstName: "Gwen",
		lastName: "Stacy",
		email: "stacyTech@gmail.com",
		password: "stacyTech1991",
		isAdmin: true
	},

	{
		firstName: "Peter",
		lastName: "Parker",
		email: "peterWebDev@gmail.com",
		password: "webDeveloperPeter",
		isAdmin: true
	},

	{
		firstName: "Jonah",
		lastName: "Jameson",
		email: "jjjameson@gmail.com",
		password: "spideyisamenace",
		isAdmin: false
	},

	{
		firstName: "Otto",
		lastName: "Octavius",
		email: "ottoOctopi@gmail.com",
		password: "docOck15",
		isAdmin: true
	}

])

// $regex- will allow us to find documents in which it will match the characters/ pattern of the character we are looking for

	db.users.find( {firstName: {$regex: 'O'} } )

	//$options

	db.users.find( {lastName: {$regex: 'o', $options: '$i'} } )

// find documents that matches a specific word

	db.users.find( {email: {$regex: 'web', $options: '$i'} } )

/*
	Mini-Activity: 4 mins

	>> using $regex look for products which name has the word razer in it
		>> it should not be case sensitive
	>> using $regex look for products which name has the word rakk in it
		>> it should not be case sensitive
	>> send your output in hangouts

*/

// S O L U T I O N

db.products.find( {name: {$regex: 'razer', $options: '$i'} } )

db.products.find( {name: {$regex: 'rakk', $options: '$i'} } )


// $or and $and

	// $or operator- logical operation wherein we can look or find a document which can satisfy at least 1 condition

	db.products.find({
		$or: [
			{
				name: {$regex: 'x', $options: '$i'}
			},
			{
				price: {$lte: 10000}
			}
		]
	})

	db.products.find({
		$or: [
			{
				name: {$regex: 'x', $options: '$i'}
			},
			{
				price: 30000
			}
		]
	})

	db.users.find({
		$or: [
			{
				firstName: {$regex: 'a', $options: '$i'}
			},
			{
				isAdmin: true
			}

		]
	})
/*
		Mini Activity: 2 mins

		>> Find a user with a last name with letter w
			>> make sure it is not case sensitive
		>> The user should not be an admin
		>> Send your screenshot in our hangouts
*/

// S O L U T I O N

db.users.find({
		$or: [
			{
				lastName: {$regex: 'w', $options: '$i'}
			},
			{
				isAdmin: false
			}
		]
	})

	db.collectionName.drop()

	db.dropDatabase()

// $and - logical operation wherein we can look or find documents which can satisfy both conditions

	db.products.find({
		$and: [
			{
				name: {$regex: 'razer', $options: '$i'}
			},
			{
				price: {$gte: 3000}
			}
		]
	})

	db.users.find({
		$and: [
			{
				lastName: {$regex: 'w', $options: '$i'}
			},
			{
				isAdmin: false
			}
		]
	})

// Field Projection
	// allows us to hide/ show properties/ field of the returned document after query

	// field projection
		// 0 means hide
		// 1 means show

	db.users.find({},{_id: 0, password: 0})

	db.users.find({isAdmin: false}, {_id: 0, email: 1})

	db.products.find(
			{
				price: {$gte: 10000}
			},
			{
				_id: 0, name: 1, price: 1
			}
		)